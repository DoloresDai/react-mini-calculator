import React, {Component} from 'react';
import './miniCalculator.less';

/*const math = {
    operationPlus: '+ 1',
    operationSubtract: '-',
    operationMultiply: '*',
    operationDivide: '/',
}*/


class MiniCalculator extends Component {
    constructor(props) {
        super(props)
        this.state = {
            num: 0,
        };
        this.plus = this.plus.bind(this);
        this.subtract = this.subtract.bind(this);
        this.multiply = this.multiply.bind(this);
        this.divide = this.divide.bind(this);
    }


    render() {
        return (
            <section>
                <div>计算结果为:
                    <span className="result"> {this.state.num} </span>
                </div>
                <div className="operations">
                    <button onClick={this.plus}>加1</button>
                    <button onClick={this.subtract}>减1</button>
                    <button onClick={this.multiply}>乘以2</button>
                    <button onClick={this.divide}>除以2</button>
                </div>
            </section>
        );
    }

    plus() {
        this.setState({
            num: this.state.num + 1,
        });
    }

    subtract() {
        this.setState({
            num: this.state.num - 1,
        });
    }

    multiply() {
        this.setState({
            num: this.state.num * 2,
        });
    }

    divide() {
        this.setState({
            num: this.state.num / 2,
        });
    }

}

export default MiniCalculator;

